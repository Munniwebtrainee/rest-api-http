import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

export class User {
  id: number;
  name: string;
  email: string;
  username: string;
}


@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiUrl = 'https://jsonplaceholder.typicode.com/users';

  constructor(private _http :HttpClient) { }

  getUsers(){
    return this._http.get<User[]>(this.apiUrl);
  }

}
